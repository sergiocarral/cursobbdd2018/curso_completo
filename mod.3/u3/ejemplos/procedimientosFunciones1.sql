﻿CREATE DATABASE procedimientoFunciones1;
USE procedimientoFunciones1;

/*crear un procedimiento almacenado que nos muestre al fecha de hoy*/


DELIMITER $$
DROP PROCEDURE IF EXISTS ej1$$

CREATE PROCEDURE ej1 ()
BEGIN
  SELECT
    NOW();
END
$$

DELIMITER ;

CALL ej1();

/*crear una variable de tipo datetime y almacenar en esa variable la fecha de hoy*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej2$$

CREATE PROCEDURE ej2 ()
BEGIN
  DECLARE v1 datetime; -- declarado
  SET v1 = NOW(); -- asignado
  SELECT
    v1; -- mostrado
END
$$

/*sumar dos argumentos*/

DELIMITER ;

CALL ej2();

DELIMITER $$
DROP PROCEDURE IF EXISTS ej3$$

CREATE PROCEDURE ej3 (arg1 int, arg2 int)
BEGIN
  DECLARE suma int;
  SET suma = arg1 + arg2;
  SELECT
    suma;
END
$$

DELIMITER ;

/*llamar al procedimiento*/
CALL ej3(3, 4);
CALL ej3(6, 4);
CALL ej3(7, 4);

/*resta*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej4$$

CREATE PROCEDURE ej4 (arg1 int, arg2 int)
BEGIN
  DECLARE resta int;
  SET resta = arg1 - arg2;
  SELECT
    resta;
END
$$

DELIMITER ;

CALL ej4(10, 3);
CALL ej4(4, 1);
CALL ej4(5, 6);

/*crear un procedimiento en que tenemos que calcular una suma y mostrarlo en una tabla en caso de que no exista*/
DELIMITER $$
DROP PROCEDURE IF EXISTS ej5$$

CREATE PROCEDURE ej5 (a1 int, a2 int)
BEGIN
  DECLARE suma int;
  -- crear la tabla para almacenar los resultados
  CREATE TABLE IF NOT EXISTS datos (
    id int AUTO_INCREMENT,
    dato1 int,
    dato2 int,
    suma int,
    PRIMARY KEY (id)
  );
  -- calcule la suma
  SET suma = a1 + a2;
  -- introducir los datos en la tabla
  INSERT INTO datos
    VALUES (DEFAULT, a1, a2, suma);
END
$$

DELIMITER ;

CALL ej5(7, 9);
CALL ej5(8, 2);

SELECT
  *
FROM datos;

/*ej32
 suma, producto 2 NUMEROS
  crear una tabla llamada ej32(id,d1,d2,suma,producto)
  introducir los datos en la tabla y los resultados*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej6$$

CREATE PROCEDURE ej6 (d1 int, d2 int)
BEGIN
  DECLARE suma int DEFAULT 0;
  DECLARE producto int DEFAULT 0;
  -- crear la tabla para almacenar los resultados
  CREATE TABLE IF NOT EXISTS ej32 (
    id int AUTO_INCREMENT,
    d1 int,
    d2 int,
    suma int,
    producto int,
    PRIMARY KEY (id)
  );
  -- calcule la suma
  SET suma = d1 + d2;
  SET producto = d1 * d2;
  -- introducir los datos en la tabla
  INSERT INTO ej32
    VALUES (DEFAULT, d1, d2, suma, producto);
END
$$

DELIMITER ;

-- llamar al procedimiento

CALL ej6(2, 4);
CALL ej6(3, 2);

SELECT
  *
FROM ej32;

DELIMITER $$
DROP PROCEDURE IF EXISTS ej7$$

CREATE PROCEDURE ej7 ()
BEGIN
  DROP TABLE IF EXISTS ej33;
  -- crear la tabla para almacenar los resultados
  CREATE TABLE ej33 (
    id int AUTO_INCREMENT,
    d1 int,
    d2 int,
    resultado int,
    PRIMARY KEY (id)
  );

END
$$

DELIMITER ;

-- llamar al procedimiento

CALL ej7();

/*realizar el cubo de un numero*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej33a$$

CREATE PROCEDURE ej33a (d1 int, d2 int)
BEGIN
  -- creando la variable
  DECLARE resultado int DEFAULT 0;
  -- calcule la potencia
  SET resultado = POW(d1, d2);

  -- introducir los datos en la tabla
  INSERT INTO ej33
    VALUES (DEFAULT, d1, d2, resultado);
END
$$

DELIMITER ;

-- llamar al procedimiento

CALL ej33a(2, 3);

SELECT
  *
FROM ej33;

/*realizar la raiz cuadrada de un numero*/+

DELIMITER $$
DROP PROCEDURE IF EXISTS ej33b $$

CREATE PROCEDURE ej33b(d1 int)
BEGIN
 DECLARE resultado int DEFAULT 0;
  -- crear la tabla para almacenar los resultados
  SET resultado=SQRT(d1);
  -- introducir los datos en la tabla
  INSERT INTO ej33 VALUES
    (DEFAULT,d1,NULL,resultado);
END
$$

DELIMITER ;

-- llamar al procedimiento

CALL ej33b(9);
SELECT
  *
FROM ej33;

/*ejercicio34*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej34$$

CREATE PROCEDURE ej34 ()
BEGIN
  DROP TABLE IF EXISTS ej34;

  CREATE TABLE ej34 (
    id int AUTO_INCREMENT,
    texto varchar(50),
    longitud int,
    caracteres varchar(50),
    PRIMARY KEY (id)
  );
END$$

DELIMITER ;

-- llamar al procedimiento

CALL ej34();

-- crear el procedimiento

DELIMITER $$
DROP PROCEDURE IF EXISTS ej34B$$

CREATE PROCEDURE ej34B (argumento varchar(50))
BEGIN
  INSERT INTO ej34 (texto)
    VALUES (argumento);
END$$
DELIMITER ;

-- llamar al procedimiento

CALL ej34B('sergio');
SELECT
  *
FROM ej34;
-- crear el procedimiento
DELIMITER $$
DROP PROCEDURE IF EXISTS ej34A$$

CREATE PROCEDURE ej34A ()
BEGIN
  UPDATE ej34
  SET longitud = LENGTH(texto);
END$$
DELIMITER ;

-- llamar al procedimiento

CALL ej34A();

SELECT
  *
FROM ej34;

-- crear el procedimiento

DELIMITER $$
DROP PROCEDURE IF EXISTS ej34C$$

CREATE PROCEDURE ej34C (caracteres int, registro int)
BEGIN
  UPDATE ej34 e
  JOIN (SELECT
      *
    FROM ej34 LIMIT registro, 1000) c1 USING (id)
  SET e.caracteres = LEFT(e.texto, caracteres);

END$$
DELIMITER ;

-- llamar al procedimiento

CALL ej34C(2, 2);

SELECT
  *
FROM ej34;

-- crear el procedimiento

DELIMITER $$
DROP PROCEDURE IF EXISTS ej35$$

CREATE PROCEDURE ej35 (n int)
BEGIN
  IF (n > 10) THEN
    SELECT
      "grande";
  ELSE
    SELECT
      "pequeño";
  END IF;

END$$
DELIMITER ;

-- llamar al procedimiento

CALL ej35(12);

-- crear el procedimiento

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo4$$

CREATE PROCEDURE ejemplo4 (n1 int, n2 int)
BEGIN
  CREATE OR REPLACE TEMPORARY TABLE ejemplo4 (
    indice int AUTO_INCREMENT,
    valor int,
    PRIMARY KEY (indice)
  );

  INSERT INTO ejemplo4
    VALUES (DEFAULT, n1),
    (DEFAULT, n2);

  SELECT
    *
  FROM ejemplo4;
END$$
DELIMITER ;

-- llamar al procedimiento

CALL ejemplo4(8, 90);

SELECT
  *
FROM EJEMPLO4;

-- valores de entrada entrada, almacenamieno, entrada_salida

DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo5$$

CREATE PROCEDURE ejemplo5 (arg1 int, OUT arg2 int, INOUT arg3 int)
BEGIN
  SELECT
    arg1,
    arg2,
    arg3;
END$$
DELIMITER ;

SET @n3 = 10;
CALL ejemplo5(1, @n2, 3);