﻿/*ejercicio1*/DELIMITER //
DROP PROCEDURE IF EXISTS ej1 //

CREATE OR REPLACE PROCEDURE ej1 (frase varchar(100),caracter char(1))
BEGIN
  IF (LOCATE(caracter,frase)=0) THEN 
    SELECT "no esta";
  ELSE  
    SELECT "esta";
    END IF;
END //

DELIMITER ;

CALL ej1("ejemplo de clase","x");

/*ejercicio2*/
USE ejemplo2;
DELIMITER //
DROP PROCEDURE IF EXISTS ej2 //

CREATE OR REPLACE PROCEDURE ej2 (texto varchar(50),caracter char(1))
BEGIN
  SELECT SUBSTRING_INDEX(texto,caracter,1);
END //

DELIMITER;

CALL ej2 ('kevin','i');

/*ejercicio 3*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3 //

CREATE OR REPLACE PROCEDURE ej3 (n1 int,n2 int,n3 int,OUT mayor int,OUT menor int)

BEGIN
  SET mayor= GREATEST(n1,n2,n3);
  SET menor= LEAST(n1,n2,n3);
END //

DELIMITER;

CALL ej3 (9,8,1,@mayor,@menor);
SELECT @mayor,@menor;

/*ejercicio4*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej4 //

CREATE OR REPLACE PROCEDURE ej4 ()

BEGIN
  DECLARE num1 int DEFAULT 0;
  DECLARE num2 int DEFAULT 0;
  
  SELECT COUNT(numero1) INTO num1 FROM datos802 WHERE numero1>50;
  SELECT COUNT(numero2) INTO num2 FROM datos802 WHERE numero2>50;
  SELECT num1,num2;
END //

DELIMITER;

CALL ej4();

/*ejercicio5*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej5 //

CREATE OR REPLACE PROCEDURE ej5 ()

BEGIN
  UPDATE datos802 SET suma=numero1+numero2;
  UPDATE datos802 SET resta=numero1-numero2;
END //

DELIMITER;

CALL ej5();
SELECT * FROM datos802;

/* Ejercicio 6 */

DELIMITER //
DROP PROCEDURE IF EXISTS ej6 //

CREATE OR REPLACE PROCEDURE ej6 ()

BEGIN
  -- limpiar campo suma y resta a null
  UPDATE datos802 SET suma=NULL,resta=NULL;
  -- si num1>num2 realizar suma
  UPDATE datos802 SET suma=IF(numero1>numero2,numero1+numero2,0);
  UPDATE datos802 SET resta=IF(numero2>numero1,numero2-numero1,IF(numero1>numero2,numero1-numero2,0));
END //

DELIMITER;

CALL ej6();
SELECT * FROM datos802;


/* ejercicio7 */

DELIMITER //
DROP PROCEDURE IF EXISTS ej7 //

CREATE OR REPLACE PROCEDURE ej7 ()

BEGIN
  UPDATE datos802 SET junto=CONCAT_WS(' ',texto1,texto2);
END //

DELIMITER;

CALL ej7();

SELECT * FROM datos802;

/* ejercicio 8 */

DELIMITER //
DROP PROCEDURE IF EXISTS ej8 //

CREATE OR REPLACE PROCEDURE ej8 ()

BEGIN
  UPDATE datos802 SET junto=NULL;

  UPDATE datos802 SET junto=IF(rango='A',CONCAT(RTRIM(texto1),'-',texto2),IF(rango='B',CONCAT(RTRIM(texto1),'+',texto2),texto1));

END //

DELIMITER;

CALL ej8();
SELECT * FROM datos802;


