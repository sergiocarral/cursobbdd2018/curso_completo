﻿SELECT nombre,nomequipo FROM ciclista WHERE edad BETWEEN 23 AND 27; -- Nombre Y equipo de los ciclistas que tienen entre 23 Y 27 años.
SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'; -- Dorsal de los ciclistas que han llevado el maillot amarillo.
SELECT nompuerto,pendiente FROM puerto WHERE altura >2000; -- Nombre y pendiente de los puertos de mas de 2000 metros.
SELECT numetapa,kms FROM etapa WHERE salida<>llegada; -- Numero de etapa y kms de las etapas donde la salida esta en una localidad distinta a la llegada.