﻿1 Listar los dorsales de los ciclista cuya edad es menor de 35{
~-25%select dorsal from ciclista WHERE edad=35
=SELECT dorsal FROM ciclista WHERE edad<35
~-25%select dorsal from ciclista WHERE edad<>35
~-25%select DISTINCT dorsal from ciclista WHERE edad<35
}

2 Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea menor que 8 y cuya altura sea mayor a 1500{
~-25%select dorsal from puerto where pendiente>8 AND altura>1500
=SELECT DISTINCT dorsal FROM puerto WHERE pendiente<8 AND altura>1500
~-25%SELECT dorsal FROM puerto WHERE pendiente<8 AND altura>1500
~-25%SELECT dorsal FROM puerto WHERE pendiente<8 OR altura>1500
}

3 Numero de ciclistas que hay en el equipo Mapei-Clas y en el equipo ONCE{
~-25%SELECT COUNT(*) AS nCiclistas,nomequipo FROM ciclista WHERE nomequipo='Mapei-Clas' OR nomequipo='ONCE'
~-25%SELECT COUNT(*) AS nCiclistas,nomequipo FROM ciclista WHERE nomequipo='Mapei-Clas' AND nomequipo='ONCE'
=SELECT COUNT(*) AS nCiclistas,nomequipo FROM ciclista WHERE nomequipo='Mapei-Clas' UNION SELECT COUNT(*) AS nCiclistas,nomequipo FROM ciclista WHERE nomequipo='ONCE'
~-25%SELECT COUNT(*) AS nCiclistas,nomequipo FROM ciclista WHERE nomequipo IN 'Mapei-Clas','ONCE'
}

4 Listar el dorsal, el nombre, el número de etapa de los cilistas que han ganado alguna etapa{
~-25%SELECT c.dorsal,c.nombre FROM ciclista c INNER JOIN etapa e ON c.dorsal=e.dorsal
=SELECT c.dorsal,c.nombre,e.numetapa FROM ciclista c INNER JOIN etapa e ON c.dorsal=e.dorsal
~-25%SELECT c.dorsal,c.nombre,e.numetapa FROM ciclista c INNER JOIN etapa e USING(dorsal)
~-25%SELECT c.dorsal,c.nombre,e.numetapa FROM ciclista c NATURAL JOIN etapa e ON c.dorsal=e.dorsal
}

5 Indicar la pendiente media de los puertos{
~25%SELECT AVG(altura) AS pendienteMedia FROM puerto
=SELECT AVG(pendiente) AS pendienteMedia FROM puerto
~25%SELECT AVG(pendiente) AS pendienteMedia FROM ciclista
~25%SELECT AVG(altura) AS alturaMedia FROM puerto
}

6  Listar el nombre de los ciclistas que empiecen por M{
=SELECT c.nombre FROM ciclista c WHERE UPPER(LEFT(nombre,1))='M'
~25%SELECT c.nombre FROM ciclista c WHERE nombre LIKE 'M%'
~25%SELECT e.nombre FROM ciclista c WHERE UPPER(LEFT(nombre,2))='M'
~25%SELECT c.nombre FROM ciclista c WHERE UPPER(LEFT(nombre,2))='M'

7  Nombre Y equipo de los ciclistas que tienen entre 23 Y 27 años.{
~-25%SELECT nombre,código FROM ciclista WHERE edad 22 Y 27 años
=SELECT nombre,nomequipo FROM ciclista WHERE edad BETWEEN 23 AND 27
~-25%SELECT nombre;nomequipo FROM ciclista WHERE edad BETWEEN 23 y 27
~-25%SELECT nombre,nomequipo FROM ciclista WHERE edad BETWEEN 23 Y 27
}

8  Dorsal de los ciclistas que han llevado el maillot amarillo.{
~-25%SELECT DISTINCT DORSAL FROM maillot WHERE codigo='MGE'
~-25%SELECT DISTINCT DORSAL FROM maillot WHERE código='MGE'
=SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'
~-25%SELECT DISTINCT dorsal FROM lleva WHERE codigo='MGE'
}

9  Nombre y pendiente de los puertos de mas de 2000 metros.{
=SELECT nompuerto,pendiente FROM puerto WHERE altura >2000
~-25%SELECT nompuerto pendiente FROM puerto WHERE altura >2000
~-25%SELECT nompuerto pendiente FROM etapa WHERE altura >2000
~-25%SELECT nompuerto,pendiente FROM puerto WHERE altura >=2000
}

10 Numero de etapa y kms de las etapas donde la salida esta en una localidad distinta a la llegada.{
~-25%SELECT numetapa,kms FROM puerto WHERE salida<>llegada
~-25%SELCT numetapa,kms FROM etapa WHERE salida<>llegada
~-25%SELECT numetapa,kms FROM étapa WHERE salida<>llegada
=SELECT numetapa,kms FROM etapa WHERE salida<>llegada
}