﻿SELECT * FROM emple;
SELECT * FROM depart;
SELECT * FROM depart;
SELECT * FROM emple ORDER BY dept_no DESC;

SELECT * FROM emple ORDER BY dept_no DESC,oficio;
SELECT * FROM emple ORDER BY dept_no DESC,apellido;
SELECT apellido,oficio FROM emple;
SELECT loc, dept_no FROM depart;
SELECT * FROM emple ORDER BY apellido;
SELECT * FROM emple ORDER BY apellido DESC;
SELECT * FROM emple WHERE oficio='analista';
SELECT * FROM emple WHERE oficio='analista' AND salario>2000;
SELECT apellido FROM emple ORDER BY oficio,apellido;
SELECT * FROM emple WHERE apellido NOT LIKE'%Z';
SELECT emp_no FROM emple WHERE salario>2000;
SELECT emp_no,apellido FROM emple WHERE salario<2000;
SELECT * FROM emple WHERE salario BETWEEN 1500 AND 2500;
SELECT apellido,oficio FROM emple WHERE dept_no=20;
SELECT * FROM emple WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' ORDER BY apellido;
SELECT * FROM emple WHERE oficio='vendedor' ORDER BY apellido;
SELECT * FROM emple WHERE dept_no=10 AND oficio='analista' ORDER BY apellido, oficio;
SELECT DISTINCT MONTH(fecha_alt) FROM emple;
SELECT DISTINCT YEAR(fecha_alt) FROM emple;
SELECT DISTINCT DAY(fecha_alt) FROM emple;
SELECT apellido FROM emple WHERE salario>2000 OR dept_no=20;
SELECT apellido FROM emple WHERE apellido LIKE 'a%' OR apellido LIKE 'm%';
SELECT * FROM emple WHERE apellido LIKE 'a%' AND oficio LIKE '%e%' ORDER BY oficio,salario DESC;
SELECT apellido FROM emple WHERE SUBSTRING(apellido,1,1)='a';
SELECT COUNT(*) FROM emple;
SELECT COUNT(*) FROM depart;
SELECT COUNT(*) FROM emple WHERE oficio='vendedor';
SELECT apellido,dnombre FROM emple JOIN depart USING(dept_no);
SELECT apellido,oficio,dnombre FROM emple JOIN depart USING(dept_no) ORDER BY apellido DESC;
SELECT apellido FROM emple WHERE salario=(SELECT MAX(salario) FROM emple);
SELECT COUNT(*) FROM emple;
SELECT COUNT(*) FROM depart;
SELECT (SELECT COUNT(*) FROM emple)+(SELECT COUNT(*) FROM depart);
SELECT dept_no,COUNT(*)nempleados FROM emple WHERE emp_no GROUP BY dept_no;

SELECT dept_no,COUNT(*) n FROM emple GROUP BY dept_no;

SELECT dnombre,IFNULL(n,0) NUMERO_DE_EMPLEADOS FROM depart LEFT JOIN (
  SELECT dept_no,COUNT(*) n FROM emple GROUP BY dept_no
) c1 USING(dept_no) ORDER BY NUMERO_DE_EMPLEADOS DESC;

