﻿SELECT persona,supervisor FROM supervisa;
SELECT DISTINCT persona FROM trabaja WHERE compañia='fagor';
SELECT DISTINCT persona FROM trabaja WHERE compañia='fagor' OR compañia='indra';
SELECT COUNT(*) FROM ciudad;
SELECT persona.nombre,población FROM persona JOIN ciudad ON ciudad = ciudad.nombre;
SELECT persona.nombre,calle,población FROM persona JOIN ciudad ON ciudad = ciudad.nombre;
SELECT COUNT(DISTINCT ciudad) FROM compania;
SELECT COUNT(DISTINCT ciudad) FROM persona;
SELECT ciudad,salario,nombre,compañia FROM persona JOIN trabaja ON nombre = persona
  ORDER BY nombre,salario DESC;
SELECT COUNT(DISTINCT persona) FROM trabaja WHERE compañia='indra';
SELECT nombre,supervisor,ciudad FROM persona JOIN supervisa ON nombre = persona ORDER BY nombre;

SELECT p1.nombre,p2.nombre,p1.ciudad,p2.ciudad FROM supervisa JOIN persona p1 ON persona=p1.nombre JOIN persona p2 ON supervisor=p2.nombre
  ORDER BY p1.nombre;

SELECT AVG(población) FROM ciudad;
SELECT nombre FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);
SELECT nombre FROM ciudad WHERE población<(SELECT AVG(población) FROM ciudad);

SELECT MAX(población) FROM ciudad;
SELECT nombre FROM ciudad WHERE población=(SELECT MAX(población) FROM ciudad);
SELECT nombre FROM ciudad WHERE población=(SELECT MIN(población) FROM ciudad);

SELECT COUNT(*) FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

SELECT ciudad,COUNT(*) n FROM persona GROUP BY ciudad;

SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia;

SELECT * FROM trabaja JOIN compania ON compañia = nombre;
SELECT persona.nombre,persona.ciudad,c1.ciudad
  FROM (SELECT * FROM trabaja JOIN compania ON compañia = nombre) c1 JOIN persona ON persona = persona.nombre;

SELECT compañia,AVG(salario) FROM trabaja GROUP BY compañia;

SELECT nombre FROM persona RIGHT JOIN trabaja ON nombre = persona;


SELECT persona FROM trabaja WHERE compañia='fagor';
SELECT nombre FROM (SELECT persona FROM trabaja WHERE compañia='fagor') c1 
  RIGHT JOIN persona ON persona = nombre WHERE persona IS NULL;
-- C1
SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia;
-- M1
SELECT MAX(n) FROM (SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia) C1;
-- solucion con where
SELECT compañia FROM (SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia) C1
 WHERE n=(SELECT MAX(n) FROM (SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia) C1); 
-- solucion con having
SELECT compañia FROM trabaja GROUP BY compañia
  HAVING 
COUNT(*)=(SELECT MAX(n) FROM (SELECT compañia,COUNT(*) n FROM trabaja GROUP BY compañia) C1);



SELECT * FROM persona;
SELECT * FROM compania;
SELECT * FROM trabaja;
SELECT * FROM ciudad;
SELECT * FROM supervisa;