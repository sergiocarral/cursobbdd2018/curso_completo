﻿SELECT 2+3;
SELECT provincia FROM provincias;
SELECT provincia,poblacion/superficie densidad FROM provincias;
SELECT SQRT(2);
SELECT DISTINCT autonomia FROM provincias;
SELECT provincia FROM provincias WHERE provincia=autonomia;
SELECT provincia FROM provincias WHERE provincia LIKE '%ue%';
SELECT provincia FROM provincias WHERE provincia LIKE 'a%';
SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE '%ana';
SELECT DISTINCT provincia FROM provincias WHERE autonomia LIKE '% %';
SELECT provincia FROM provincias WHERE provincia LIKE '% %';
SELECT DISTINCT autonomia,CHAR_LENGTH(autonomia) FROM provincias ORDER BY autonomia DESC;
SELECT DISTINCT provincia FROM provincias WHERE provincia NOT LIKE '% %';
SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE '% %' ORDER BY autonomia DESC;
SELECT DISTINCT autonomia FROM provincias WHERE autonomia NOT LIKE '% %' ORDER BY autonomia DESC;
SELECT DISTINCT autonomia FROM provincias WHERE provincia LIKE '% %' ORDER BY autonomia;
SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE 'CAN%' ORDER BY autonomia;
SELECT DISTINCT autonomia FROM provincias WHERE poblacion >1e6 ORDER BY autonomia;
SELECT provincia FROM provincias WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
UNION
SELECT autonomia FROM provincias WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;
SELECT SUM(poblacion) FROM provincias;
SELECT COUNT(*) FROM provincias;
SELECT DISTINCT autonomia FROM provincias WHERE LOCATE(provincia,autonomia)>0;
SELECT DISTINCT autonomia FROM provincias WHERE autonomia NOT LIKE '% %' ORDER BY autonomia DESC;
SELECT DISTINCT autonomia FROM provincias WHERE provincia LIKE '% %' ORDER BY autonomia;
SELECT provincia FROM provincias WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
  UNION
SELECT autonomia FROM provincias WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;
SELECT SUM(superficie) FROM provincias;
SELECT MIN(provincia) FROM provincias;
SELECT provincia FROM provincias WHERE CHAR_LENGTH(provincia)>CHAR_LENGTH(autonomia);
SELECT COUNT(DISTINCT autonomia) FROM provincias;
SELECT MIN(CHAR_LENGTH(autonomia)) FROM provincias;
SELECT MAX(CHAR_LENGTH(provincia)) FROM provincias;
SELECT ROUND(AVG(poblacion)) FROM provincias WHERE poblacion BETWEEN 2e6 AND 3e6;