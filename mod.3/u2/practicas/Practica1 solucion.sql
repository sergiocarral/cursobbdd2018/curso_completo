﻿USE PRACTICA1; -- selecciono la base de datos con la que voy a trabajar

/* CONSULTA1 */

SELECT e.emp_no,
       e.apellido,
       e.oficio,
       e.dir,
       e.fecha_alt,
       e.salario,
       e.comision,
       e.dept_no FROM emple e;

/* CONSULTA2 */

  SELECT d.dept_no,
         d.dnombre,
         d.loc FROM depart d;

  /* CONSULTA3 */

  SELECT
   e.apellido, e.oficio
  FROM
    emple e;

  /* CONSULTA4 */

    SELECT d.dept_no,
           d.loc FROM depart d;

    /* CONSULTA5 */

      SELECT d.dept_no, d.dnombre, d.loc
        FROM depart d;

      /* CONSULTA6 */

        SELECT
          COUNT(*) numeroEmpleados
          FROM
          emple e

