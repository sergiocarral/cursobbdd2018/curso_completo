﻿SELECT DISTINCT ciclista.nomequipo FROM equipo JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo JOIN etapa ON ciclista.dorsal = etapa.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa;

/*VAMOS A REALIZARLA POR PARTES*/
   --C1
  SELECT DISTINCT etapa.numetapa,etapa.dorsal FROM etapa JOIN puerto USING(numetapa);
   --C2
  SELECT DISTINCT nomequipo FROM ciclista JOIN ( SELECT DISTINCT etapa.numetapa,etapa.dorsal FROM etapa JOIN puerto USING(numetapa)) AS C1 ON ciclista.dorsal=C1.dorsal;
   --CONSULTA FINAL
SELECT * FROM equipo JOIN(SELECT nomequipo FROM ciclista JOIN ( SELECT DISTINCT etapa.numetapa,etapa.dorsal FROM etapa JOIN puerto USING(numetapa)) AS C1 ON ciclista.dorsal=C1.dorsal) AS C2
  JOIN (SELECT DISTINCT etapa.numetapa,etapa.dorsal FROM etapa JOIN puerto USING(numetapa)) AS C1
