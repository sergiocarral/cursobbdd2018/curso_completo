﻿CREATE TABLE IF NOT EXISTS importes(
  tipo varchar(9),
  valor int,
  PRIMARY KEY (tipo)
  );

INSERT INTO importes (tipo,valor)
  VALUES ('jornalero',25),('efectivo',250);

ALTER TABLE empleados ADD COLUMN salarioBasico float;
UPDATE empleados JOIN importes ON Tipo_trabajo = tipo
  SET salarioBasico = valor* IF(Tipo_trabajo='jornalero',Horas_trabajadas,Dias_trabajdos);

ALTER TABLE empleados ADD COLUMN premios float;
UPDATE empleados SET premios = salarioBasico* IF(Dias_trabajdos>20 OR Horas_trabajadas>200,IF(rubro='chofer'OR rubro='azafata',0.15,0.05),0);

ALTER TABLE empleados ADD COLUMN sueldoNominal float;
UPDATE empleados SET sueldoNominal = salarioBasico + premios;

--ALTER TABLE empleados DROP COLUMN bps_irp;
ALTER TABLE empleados ADD COLUMN bps float;
UPDATE empleados SET bps = sueldoNominal*0.13;

ALTER TABLE empleados ADD COLUMN irp float;
UPDATE empleados SET irp = sueldoNominal*
  CASE
    WHEN sueldoNominal<4*1160 THEN 0.03
    WHEN sueldoNominal<=10*1160 THEN 0.06
    WHEN sueldoNominal>10*1160 THEN 0.09
END;

ALTER TABLE empleados ADD COLUMN subtotalDescuentos float;
UPDATE empleados SET subtotalDescuentos = bps+irp;

ALTER TABLE empleados ADD COLUMN sueldoLiquido float;
UPDATE empleados SET sueldoLiquido = sueldoNominal-subtotalDescuentos;

ALTER TABLE empleados ADD COLUMN v_transporte float;
UPDATE empleados SET v_transporte = 
  CASE
    WHEN (DATEDIFF(NOW(),`Fecha-nac`)/365>40) AND (NOT Barrio='condon' OR Barrio='centro') THEN 200
    WHEN (DATEDIFF(NOW(),`Fecha-nac`)/365>40) AND (Barrio='condon' OR Barrio='centro') THEN 150
    WHEN (DATEDIFF(NOW(),`Fecha-nac`)/365<40) THEN 100
  END;

ALTER TABLE empleados ADD COLUMN valimentacion float;
UPDATE empleados SET valimentacion =
  CASE
    WHEN sueldoLiquido<5000 THEN 300
    WHEN sueldoLiquido<10000 AND (Rubro='jornaleros') THEN 200
    WHEN sueldoLiquido>10000 AND (Rubro='efectivos') THEN 100
  ELSE 0
  END;