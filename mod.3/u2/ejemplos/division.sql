﻿SELECT dorsal,COUNT(*) N
  FROM (SELECT DISTINCT dorsal,código FROM lleva) c1
  GROUP BY dorsal
  HAVING N=(SELECT COUNT(DISTINCT código) FROM lleva);