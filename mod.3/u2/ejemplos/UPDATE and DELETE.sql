﻿SELECT dato1,
       dato2,
       dato3 FROM persona;

UPDATE persona SET dato1=0;
UPDATE persona SET dato2=0,dato3=0;

SELECT * FROM persona WHERE LEFT(nombre,1)='c';
UPDATE persona SET dato1=1 WHERE LEFT(nombre,1)='c';

SELECT * FROM persona WHERE QUARTER(fecha)=1;
UPDATE persona SET dato3=dato1+dato2 WHERE QUARTER(fecha)=1;

--poner un cero en el campo trabajadores

UPDATE empresas SET trabajadores=0;

--el campo trabajadores me indique cuantos trabajadores tiene esa empresa

--consulta de seleccion
SELECT empresa,COUNT(*) n FROM persona JOIN empresas ON empresas.codigo = persona.empresa GROUP BY empresa;

--consulta de actualización

UPDATE empresas JOIN(SELECT empresa,COUNT(*) n FROM persona JOIN empresas ON empresas.codigo = persona.empresa GROUP BY empresa) c1 ON c1.empresa = empresas.codigo;

SELECT * FROM empresas JOIN(UPDATE empresas JOIN(SELECT empresa,COUNT(*) n FROM persona JOIN empresas ON empresas.codigo = persona.empresa GROUP BY empresa) c1 ON c1.empresa = empresas.codigo)