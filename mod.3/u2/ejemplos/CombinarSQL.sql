﻿--EJEMPLOS DE CLASE PARA COMBINAR TABLAS...
SELECT * FROM ciclista,puerto;

SELECT * FROM ciclista INNER JOIN puerto USING(dorsal);

SELECT * FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal;

SELECT * FROM ciclista,puerto WHERE ciclista.dorsal=puerto.dorsal;

SELECT * FROM ciclista NATURAL JOIN puerto;
COMBINAR 3 TABLAS...
SELECT * FROM equipo JOIN ciclista ON ciclista.nomequipo=equipo.nomequipo JOIN etapa ON ciclista.dorsal=etapa.dorsal;
SELECT * FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal);