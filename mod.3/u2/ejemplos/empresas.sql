﻿--empresas

DROP DATABASE IF EXISTS empresas;

CREATE DATABASE IF NOT EXISTS empresas;
USE empresas;



CREATE TABLE persona(
  id int AUTO_INCREMENT,
  nombre varchar(15),
  apellido varchar(15),
  poblacion varchar(15),
  fecha date,
  dato1 varchar(50),
  dato2 varchar(50),
  dato3 varchar(50),
  fecha_trabaja date,
  empresa int,
  PRIMARY KEY (id)
);

CREATE TABLE empresas(
  codigo int AUTO_INCREMENT,
  poblacion varchar(15),
  cp varchar(6),
  trabajadores numeric(4),
  PRIMARY KEY (codigo)
);

ALTER TABLE persona
  ADD CONSTRAINT fkPersonaEmpresas FOREIGN KEY (empresa)
  REFERENCES empresas(codigo) ON DELETE CASCADE ON UPDATE CASCADE;


