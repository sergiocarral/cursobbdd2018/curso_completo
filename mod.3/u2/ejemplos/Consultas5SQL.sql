﻿SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL;

SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL;


SELECT DISTINCT director FROM equipo JOIN ciclista USING (nomequipo) JOIN etapa USING(dorsal);

SELECT DISTINCT director FROM equipo JOIN (
    SELECT DISTINCT ciclista.nomequipo  FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL
  ) AS c1 using(nomequipo);

SELECT director FROM (
    SELECT DISTINCT director FROM equipo JOIN (
      SELECT DISTINCT ciclista.nomequipo  FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL
    ) AS c1 using(nomequipo)
) AS c1 LEFT JOIN (
  SELECT DISTINCT director FROM equipo JOIN ciclista USING (nomequipo) JOIN etapa USING(dorsal)
) AS c2 USING(director) WHERE c2.director IS NULL;


SELECT DISTINCT equipo.director FROM equipo LEFT JOIN (
  SELECT DISTINCT director FROM equipo JOIN ciclista USING (nomequipo) JOIN etapa USING(dorsal)
  ) AS c2 USING(director) WHERE c2.director IS NULL;





SELECT DISTINCT ciclista.dorsal,nombre FROM ciclista LEFT JOIN lleva USING(dorsal) WHERE lleva.dorsal IS NULL;

SELECT DISTINCT dorsal FROM lleva WHERE código='mge';
SELECT DISTINCT ciclista.dorsal,nombre FROM ciclista LEFT JOIN (SELECT DISTINCT dorsal FROM lleva WHERE código='mge') c1 USING (dorsal) WHERE c1.dorsal IS NULL;

SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL;

SELECT DISTINCT AVG(kms) FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL;

SELECT DISTINCT COUNT(ciclista.dorsal) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL;

SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL;

SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto USING(numetapa) WHERE puerto.numetapa IS NULL;

CREATE OR REPLACE VIEW c1 AS
SELECT DISTINCT dorsal FROM etapa;

CREATE OR REPLACE VIEW c2 AS
SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto USING (numetapa);

SELECT c1.dorsal FROM c1 LEFT JOIN c2 USING (dorsal) WHERE c2.dorsal IS NULL;

DROP VIEW c1;
DROP VIEW c2;






