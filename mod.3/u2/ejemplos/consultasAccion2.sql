﻿ALTER TABLE productos ADD COLUMN descuento1 float;
SELECT * FROM productos WHERE rubro='verduras';
UPDATE productos SET descuento1 = importe_base*0.10 WHERE rubro='verduras';

ALTER TABLE productos ADD COLUMN descuento2 float;
SELECT * FROM productos WHERE `u/medida`='atado';
UPDATE productos SET descuento2 = importe_base*0.20 WHERE `u/medida`='atado';
UPDATE productos SET descuento2 = importe_base*0.05 WHERE NOT `u/medida`='atado';

ALTER TABLE productos ADD COLUMN descuento3 float;
SELECT (importe_base*0.20) descuento3 FROM productos WHERE rubro='frutas' AND importe_base >15;
UPDATE productos SET descuento3 = importe_base*0.20 WHERE rubro='frutas' AND importe_base >15;
UPDATE productos SET descuento3=0 WHERE NOT (rubro='frutas' AND importe_base >15);

ALTER TABLE productos ADD COLUMN descuento4 float;
SELECT importe_base*0.50 FROM productos WHERE granja='primavera' OR granja='litoral';
UPDATE productos SET descuento4 = importe_base*0.5 WHERE (granja='primavera' OR granja='litoral');
UPDATE productos SET descuento4 = importe_base*0.25 WHERE NOT (granja='primavera' OR granja='litoral');

ALTER TABLE productos ADD COLUMN aumento1 float;
SELECT importe_base*0.10 FROM productos WHERE (granja='la garota' OR granja='la pocha') AND (rubro='frutas' OR rubro='verduras');
UPDATE productos SET aumento1 = importe_base*0.10 WHERE (granja='la garota' OR granja='la pocha') AND (rubro='frutas' OR rubro='verduras');
UPDATE productos SET aumento1 = 0 WHERE NOT (granja='la garota' OR granja='la pocha') AND (rubro='frutas' OR rubro='verduras');

ALTER TABLE productos ADD COLUMN presentacion float;
SELECT * FROM productos WHERE `u/medida`='atado';
SELECT * FROM productos WHERE `u/medida`='unidad';
SELECT * FROM productos WHERE `u/medida`='kilo';

UPDATE productos SET presentacion=1 WHERE `u/medida`='atado';
UPDATE productos SET presentacion=2 WHERE `u/medida`='unidad';
UPDATE productos SET presentacion=3 WHERE `u/medida`='kilo';

ALTER TABLE productos DROP COLUMN categoria;

ALTER TABLE productos ADD COLUMN categoria char(1);

UPDATE productos SET categoria=IF (importe_base<10,'A',IF(importe_base BETWEEN 10 AND 20,'B','C'));

ALTER TABLE productos ADD COLUMN aumento2 float;

UPDATE productos SET categoria=IF (

 