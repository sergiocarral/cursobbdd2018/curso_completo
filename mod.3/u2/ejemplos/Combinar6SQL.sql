﻿SELECT COUNT(IdVendedor) FROM vendedores WHERE MONTH(FechaAlta)=2;

SELECT COUNT(IdVendedor) FROM vendedores WHERE `Guap@` IS NOT NULL;

SELECT MAX(Precio) FROM productos;
SELECT NomProducto FROM productos WHERE Precio=(SELECT MAX(Precio) FROM productos);

SELECT AVG(Precio),IdGrupo FROM productos GROUP BY IdGrupo;

SHOW TABLES;
SELECT COUNT(`Guap@`) FROM vendedores;
SELECT * FROM grupos;

SELECT DISTINCT `Cod Producto` FROM ventas;
SELECT DISTINCT IdGrupo FROM (
    SELECT DISTINCT `Cod Producto` FROM ventas
  ) c1 JOIN productos ON `Cod Producto`=IdProducto;

SELECT NombreGrupo FROM (
    SELECT DISTINCT IdGrupo FROM (
        SELECT DISTINCT `Cod Producto` FROM ventas
      ) c1 JOIN productos ON `Cod Producto`=IdProducto  
  ) c2 JOIN grupos USING(IdGrupo);