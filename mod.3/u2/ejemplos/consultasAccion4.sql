﻿ALTER TABLE alquileres 
  ADD CONSTRAINT fkalquilaCliente FOREIGN KEY (cliente)
  REFERENCES clientes(codigo) ON DELETE CASCADE ON UPDATE CASCADE, 
  ADD CONSTRAINT fkalquilaBici FOREIGN KEY (bici)
  REFERENCES bicis(bicis_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE averias
  ADD CONSTRAINT fkaveriaBici FOREIGN KEY (bici)
  REFERENCES bicis(bicis_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE bicis;
  SELECT bici,SUM(kms) n FROM alquileres GROUP BY bici;
  UPDATE
    bicis LEFT JOIN (SELECT bici,SUM(kms) n FROM alquileres GROUP BY bici) c1 ON bicis_id = bici SET kms = IFNULL(n,0);
