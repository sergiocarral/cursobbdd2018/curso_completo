﻿SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal;

SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal;

SELECT * FROM (SELECT DISTINCT nombre, edad FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal) AS c1 NATURAL JOIN (SELECT DISTINCT nombre, edad FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal) AS C2;

SELECT DISTINCT nomequipo FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal;
SELECT DISTINCT director FROM (SELECT DISTINCT nomequipo FROM ciclista JOIN etapa ON ciclista.dorsal = etapa.dorsal) AS C1 JOIN equipo USING(nomequipo);

SELECT DISTINCT lleva.dorsal,nombre FROM ciclista JOIN lleva USING(dorsal);

SELECT DISTINCT dorsal FROM lleva WHERE código='mge';
SELECT DISTINCT ciclista.dorsal,nombre FROM ciclista JOIN (SELECT DISTINCT dorsal FROM lleva WHERE código='mge') c1 USING(dorsal);

CREATE OR REPLACE VIEW c1 AS       
  SELECT DISTINCT dorsal FROM lleva;
CREATE OR REPLACE VIEW c2 AS
  SELECT DISTINCT dorsal FROM etapa;
SELECT * FROM c1 NATURAL JOIN c2;
DROP VIEW c1;
DROP VIEW c2;

SELECT DISTINCT numetapa FROM puerto;
--1.9 MAL (INTENTAR OTRA VEZ) respuesta 200 Y 190
CREATE OR REPLACE VIEW c1 AS
SELECT DISTINCT numetapa FROM puerto;
CREATE OR REPLACE VIEW c2 AS
SELECT dorsal FROM ciclista WHERE nomequipo='banesto';
CREATE OR REPLACE VIEW c3 AS
SELECT numetapa FROM etapa JOIN c2 USING(dorsal);
CREATE OR REPLACE VIEW c4 AS
SELECT * FROM c3 NATURAL JOIN c1;
SELECT DISTINCT kms FROM c4 JOIN etapa;
DROP VIEW c1;
DROP VIEW c2;
DROP VIEW c3;
DROP VIEW c4;

SELECT COUNT(DISTINCT etapa.dorsal) AS nciclistas FROM puerto JOIN etapa USING(numetapa);

CREATE OR REPLACE VIEW c1 AS
SELECT dorsal FROM ciclista WHERE nomequipo='banesto';
SELECT nompuerto FROM puerto JOIN c1 USING(dorsal);
DROP VIEW c1;

CREATE OR REPLACE VIEW c1 AS
SELECT dorsal FROM ciclista WHERE nomequipo='banesto';

SELECT COUNT(DISTINCT etapa.numetapa) FROM c1 JOIN etapa USING(dorsal) JOIN puerto ON  etapa.numetapa = puerto.numetapa WHERE kms>200;
DROP VIEW c1;