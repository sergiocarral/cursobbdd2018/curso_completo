﻿/*hoja7 - unidad 1 - modulo3*/

--eliminar la base de datos si existe 
  DROP DATABASE IF EXISTS hoja7unidad1modulo3;

CREATE DATABASE hoja7unidad1modulo3;
USE hoja7unidad1modulo3;
  CREATE TABLE EMPLEADO(
    dni char(9),
    PRIMARY KEY (dni)
    );

  INSERT INTO EMPLEADO VALUES
    ('dni1'),
    ('dni2');

  
  CREATE TABLE DEPARTAMENTO(
    codDepartamento varchar(15),
    PRIMARY KEY (codDepartamento)
    );

  INSERT INTO DEPARTAMENTO VALUES
    ('dep1'),
    ('dep2');

  --crear la tabla pertenece

  --eliminar la tabla
  DROP TABLE IF EXISTS pertenece;

  --crear la tabla pertenece
  CREATE TABLE IF NOT EXISTS pertenece (
    departamento varchar(15),
    empleado char(9),
    PRIMARY KEY (departamento,empleado),
    CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
    CONSTRAINT FKPerteneceEmpleado FOREIGN KEY (empleado)
      REFERENCES empleado(dni) ON DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKPerteneceDepartamento FOREIGN KEY (departamento)
      REFERENCES DEPARTAMENTO(codDepartamento) ON DELETE CASCADE ON UPDATE CASCADE
    );

  -- tabla projecto...
    DROP TABLE IF EXISTS PROYECTO;
  
    CREATE TABLE IF NOT EXISTS PROYECTO(
      codigo varchar(9),
      PRIMARY KEY (codigo)
      );

    INSERT INTO PROYECTO VALUES
      ('cod00001'),
      ('cod00002');

    --tabla trabaja...

    DROP TABLE IF EXISTS TRABAJA;

    CREATE TABLE IF NOT EXISTS TRABAJA(
      empleado char(9),
      proyecto varchar(15),
      fecha date,
      PRIMARY KEY (empleado,proyecto),
      CONSTRAINT fkTrabajaEMPLEADO FOREIGN KEY (empleado)
      REFERENCES empleado (dni)
      ON DELETE CASCADE on UPDATE CASCADE,
      CONSTRAINT fkTrabajaProyecto FOREIGN KEY (proyecto)
      REFERENCES proyecto(codigo)
      ON DELETE CASCADE on UPDATE CASCADE
      );

