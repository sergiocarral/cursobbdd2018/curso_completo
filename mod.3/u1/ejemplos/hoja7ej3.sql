﻿--hoja7ejercicio3

DROP DATABASE IF EXISTS hoja7ejercicio3;

CREATE DATABASE hoja7ejercicio3;
USE hoja7ejercicio3;

CREATE TABLE vuelo(
  numero char(4),
  fecha date,
  destino varchar(15),
  PRIMARY KEY (numero)
  );

CREATE TABLE turista(
  T char(4),
  nombre varchar(15),
  telefono numeric(10),
  PRIMARY KEY (T)
  );

CREATE TABLE agencia(
  S char(4),
  direccion varchar(15),
  telefono numeric(10),
  PRIMARY KEY (S)
  );

CREATE TABLE hotel(
  H char(4),
  plazas varchar(4),
  ciudad varchar(15),
  telefono numeric(10),
  PRIMARY KEY (H)
  );

CREATE TABLE IF NOT EXISTS toma(
  numero char(4),
  T char(4),
  clase varchar(15),
  PRIMARY KEY (numero,T),
  CONSTRAINT fkNumeroVuelo FOREIGN KEY (numero)
  REFERENCES vuelo(numero) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkvuelaturista FOREIGN KEY (T)
  REFERENCES turista(T) on DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE IF NOT EXISTS contrata(
  T char(4),
  S char(4),
  PRIMARY KEY (S,T),
  CONSTRAINT fkContrataTurista FOREIGN KEY (T)
  REFERENCES turista(T) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkContrataAgencia FOREIGN KEY (S)
  REFERENCES agencia(S) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE IF NOT EXISTS reserva(
  T char(4),
  H char(4),
  fecha_e date,
  PRIMARY KEY (T,H),
  CONSTRAINT fkReservaTurista FOREIGN KEY (T)
  REFERENCES turista(T) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkReservaHotel FOREIGN KEY (H)
  REFERENCES hotel(H) ON DELETE CASCADE ON UPDATE CASCADE
  );



