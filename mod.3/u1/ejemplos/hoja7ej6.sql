﻿--hoja7ejercicio6

DROP DATABASE IF EXISTS hoja7ejercicio6;

CREATE DATABASE hoja7ejercicio6;
USE hoja7ejercicio6;

CREATE TABLE opcion(
  nombre char(15),
  descripcion varchar(50),
  PRIMARY KEY (nombre)
  );

CREATE TABLE modelo(
  marca char(15),
  cilindrada numeric(4),
  modelo varchar(30),
  precio numeric(7),
  PRIMARY KEY (marca,cilindrada,modelo)
  );

CREATE TABLE cliente(
  dni char(9),
  nombre varchar(15),
  direccion varchar(30),
  telefono numeric(11),
  PRIMARY KEY (dni)
  );

CREATE TABLE vendedor(
  dni char(9),
  nombre varchar(15),
  direccion varchar(30),
  telefono numeric(11),
  PRIMARY KEY (dni)
  );

CREATE TABLE vehiculo(
  matricula char(8),
  marca varchar(15),
  modelo varchar(30),
  precio numeric(7),
  PRIMARY KEY (matricula)
  );

CREATE TABLE IF NOT EXISTS tiene(
  opcion char(15),
  marca char(15),
  cilindrada numeric(4),
  modelo varchar(30),
  PRIMARY KEY (opcion,marca,cilindrada,modelo),
  FOREIGN KEY (marca,cilindrada,modelo),
  CONSTRAINT fkTieneOpcion FOREIGN KEY (opcion)
  REFERENCES opcion(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkTieneModelo FOREIGN KEY (marca,cilindrada,modelo)
  REFERENCES modelo(marca,cilindrada,modelo) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE IF NOT EXISTS compra(
  marca char(15),
  cilindrada numeric(4),
  modelo varchar(30),
  cliente char(9),
  vendedor char(9),
  opcion char(15),
  matricula numeric(8),
  fecha date,
  PRIMARY KEY (marca,cilindrada,modelo,cliente,vendedor,opcion),
  
  );

CREATE TABLE IF NOT EXISTS cede(
  cliente char(9),
  vehiculo char(8),
  fecha date,
  
  );

