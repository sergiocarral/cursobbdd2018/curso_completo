﻿-- HOJA 7 EJERCICIO 2
  DROP DATABASE IF EXISTS hoja7ejercicio2;

  CREATE DATABASE hoja7ejercicio2;
   USE hoja7ejercicio2;

CREATE TABLE compañia(
  numero char(4),
  actividad varchar(50),
  PRIMARY KEY (numero)
  );

CREATE TABLE soldado(
  S char(4),
  nombre varchar(10),
  apellidos varchar(15),
  grado varchar(15),
  PRIMARY KEY (S)
  );

CREATE TABLE servicio(
  SE char(4),
  descripcion varchar(50),
  PRIMARY KEY (SE)
  );

CREATE TABLE cuartel(
  CU char(4),
  nombre varchar(10),
  PRIMARY KEY (CU)
  );

CREATE TABLE cuerpo(
  C char(4),
  denominacion varchar(50),
  PRIMARY KEY (C)
  );

CREATE TABLE perteneceCompañia(
  compañia char(4),
  soldado char(4),
  PRIMARY KEY (compañia,soldado),
  CONSTRAINT fkPerteneceCompañia FOREIGN KEY (compañia)
  REFERENCES compañia(numero) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkPerteneceSoldado FOREIGN KEY (soldado)
  REFERENCES soldado(S) on DELETE CASCADE ON UPDATE CASCADE 
  );

CREATE TABLE realiza(
  soldado char(4),
  servicio char(4),
  fecha date,
  PRIMARY KEY (soldado,servicio),
  CONSTRAINT fkRealizaSoldado FOREIGN KEY (soldado)
  REFERENCES soldado(S) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkRealizaServicio FOREIGN KEY (servicio)
  REFERENCES servicio(SE) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE esta(
  soldado char(4),
  cuartel char(4),
  PRIMARY KEY (soldado,cuartel),
  CONSTRAINT fkEstaSoldado FOREIGN KEY (soldado)
  REFERENCES soldado(S) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkEstaCuartel FOREIGN KEY (cuartel)
  REFERENCES cuartel(CU) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE perteneceCuerpo(
  cuerpo char(4),
  soldado char(4),
  PRIMARY KEY (cuerpo,soldado),
  CONSTRAINT fkPerteneceCuerpo FOREIGN KEY (cuerpo) REFERENCES cuerpo(C) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkperetenececuartel FOREIGN KEY (soldado) REFERENCES soldado(S) ON DELETE CASCADE ON UPDATE CASCADE
  );
