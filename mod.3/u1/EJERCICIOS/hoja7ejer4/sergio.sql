﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 4
*/

-- base de datos

  DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio4;

  CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3ejercicio4;

  USE hoja7unidad1modulo3ejercicio4;

CREATE TABLE IF NOT EXISTS colabora(
  alumno varchar(15),
  profesor varchar(15),
  PRIMARY KEY (alumno,profesor),
  CONSTRAINT fkalumnocolabora FOREIGN KEY (alumno)
  REFERENCES alumno ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkprofesorcolabora FOREIGN KEY (profesor)
  REFERENCES profesor ON DELETE CASCADE ON UPDATE CASCADE
  );
